from math import sqrt

def areaCirculo(raio: float):
    return 3.14 * raio * raio


def areaLateralCone(raio: float, geratriz: float):
    return 3.14 * raio * geratriz


def volumeCone(raio: float, altura: float):
    return areaCirculo(raio) * altura / 3.0


def geratrizCone(raio: float, altura: float):
    return sqrt(raio * raio + altura * altura)


def geratrizTroncoCone(raio1: float, raio2: float, altura: float):
    return sqrt((raio1 - raio2) * (raio1 - raio2) + altura * altura)


def areaEsfera(raio: float):
    return 4 * 3.14 * raio * raio


def volumeEsfera(raio: float):
    return 4 * 3.14 * raio * raio * raio / 3.0


def areaLateralCilindo(raio: float, altura: float):
    return 2 * 3.14 * raio * altura


def areaLateralTroncoCone(geratriz: float, raio1: float, raio2: float):
    return 3.14 * geratriz * (raio2 + raio1)


def volumeCilindo(raio: float, altura: float):
    return areaCirculo(raio) * altura


def volumeTroncoCone(raio1: float, raio2: float, altura: float):
    return (altura * 3.14 * (raio1 * raio1 + raio1 * raio2 + raio2 * raio2)) / 3.0


def alturaCone(raio: float, geratriz: float):
    return sqrt(geratriz * geratriz - raio * raio)