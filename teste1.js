function areaCirculo(raio) {
    return Math.PI * raio * raio;
}

function areaLateralCilindro(raio, altura) {
    return 2 * Math.PI * raio * altura;
}

function volumeCilindro(raio, altura) {
    return Math.PI * raio * raio * altura;
}

const areaLateralCone = (raio, geratriz) => Math.PI * raio * geratriz;
const areaLateralTroncoCone = (raio1, raio2, geratriz) => Math.PI * geratriz * (raio1 + raio2);
const volumeCone = (raio, altura) => Math.PI * raio * raio * altura / 3;
const volumeTroncoCone = (raio1, raio2, altura) => Math.PI * altura * (raio1 * raio1 + raio1 * raio2 + raio2 * raio2) / 3;

const areaEsfera = (raio) => 4 * Math.PI * raio * raio;
const volumeEsfera = (raio) => 4 * Math.PI * raio * raio * raio / 3;

function geratrizCone(raio, altura) {
    return Math.sqrt(raio * raio + altura * altura);
}

function geratrizTroncoCone(raio1, raio2, altura) {
    return Math.sqrt((raio1 - raio2) * (raio1 - raio2) + altura * altura);
}

const raio = 17;
const altura = 25;

function questao1() {
    console.log('------------------------------')
    console.log('Questão 1')
    const geratriz = geratrizCone(raio, altura);
    const areaB = areaCirculo(raio);
    const areaL = areaLateralCone(raio, geratriz);
    const volume = volumeCone(raio, altura);

    console.log(`g = ${geratriz}`);
    console.log(`Sb = ${areaB}`);
    console.log(`Sl = ${areaL}`);
    console.log(`St = ${areaB + areaL}`);
    console.log(`V = ${volume}`);
}

function questao2() {
    console.log('------------------------------')
    console.log('Questão 2')
    const raio = 26;
    const volume = volumeEsfera(raio);
    const area = areaEsfera(raio);
    console.log(`Area = ${area}`);
    console.log(`Volume = ${volume}`);
}

function questao3() {
    console.log('------------------------------')
    console.log('Questão 3')
    const raio = 9;
    const altura = 36;
    const base = areaCirculo(raio);
    const lateral = areaLateralCilindro(raio, altura);
    const total = 2 * base + lateral;
    const volume = volumeCilindro(raio, altura);

    console.log(`Area base = ${base}`);
    console.log(`Area lateral = ${lateral}`);
    console.log(`Area total = ${total}`);
    console.log(`Volume = ${volume}`);
}

function questao4() {
    console.log('------------------------------')
    console.log('Questão 4')
    const raio1 = 24;
    const raio2 = 15;
    const altura = 12;
    const geratriz = geratrizTroncoCone(raio1, raio2, altura);
    const base1 = areaCirculo(raio1);
    const base2 = areaCirculo(raio2);
    const lateral = areaLateralTroncoCone(raio1, raio2, geratriz);
    const volume = volumeTroncoCone(raio1, raio2, altura);

    console.log(`Geratriz = ${geratriz}`);
    console.log(`Area base1 = ${base1}`);
    console.log(`Area base2 = ${base2}`);
    console.log(`Area lateral = ${lateral}`);
    console.log(`Area total = ${base1 + base2 + lateral}`);
    console.log(`Volume = ${volume}`);
}

questao1();
questao2();
questao3();
questao4();
