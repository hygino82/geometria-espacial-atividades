function areaCirculo(raio) {
    return 3.14 * raio * raio;
}

function areaLateralCilindro(raio, altura) {
    return 2 * 3.14 * raio * altura;
}

function volumeCilindro(raio, altura) {
    return 3.14 * raio * raio * altura;
}

const areaLateralCone = (raio, geratriz) =>
    3.14 * raio * geratriz;
const areaLateralTroncoCone = (raio1, raio2, geratriz) =>
    3.14 * geratriz * (raio1 + raio2);
const volumeCone = (raio, altura) =>
    3.14 * raio * raio * altura / 3;
const volumeTroncoCone = (raio1, raio2, altura) =>
    3.14 * altura * (raio1 * raio1 + raio1 * raio2 + raio2 * raio2) / 3;

const areaEsfera = (raio) =>
    4 * 3.14 * raio * raio;
const volumeEsfera = (raio) =>
    4 * 3.14 * raio * raio * raio / 3;

function geratrizCone(raio, altura) {
    return (Math.sqrt(raio * raio + altura * altura)).toFixed(2);
}

function geratrizTroncoCone(raio1, raio2, altura) {
    return (Math.sqrt((raio1 - raio2) * (raio1 - raio2) + altura * altura)).toFixed(2);
}

function questao1(t) {
    const raio = 17 + t;
    const altura = 25 + t;
    const geratriz = geratrizCone(raio, altura);
    const base = areaCirculo(raio);
    const lateral = areaLateralCone(raio, geratriz);
    const total = base + lateral;
    const volume = volumeCone(raio, altura);

    console.log(`g = ${geratriz}`);
    console.log(`Sb = ${base}`);
    console.log(`Sl = ${lateral}`);
    console.log(`St = ${base + lateral}`);
    console.log(`V = ${volume}`);
}

function questao2(t) {
    const geratriz = 14 + t;
    const raio = geratriz / 2.0;
    const altura = raio * 1.73;

    const base = areaCirculo(raio);
    const lateral = areaLateralCone(raio, geratriz);
    const total = base + lateral;
    const volume = volumeCone(raio, altura);

    console.log(`h = ${altura}`);
    console.log(`Sb = ${base}`);
    console.log(`Sl = ${lateral}`);
    console.log(`St = ${base + lateral}`);
    console.log(`V = ${volume}`);
}

function questao3(t) {
    const altura = 36 * t;
    const raio = 9 * t;
    const base = areaCirculo(raio);
    const lateral = areaLateralCilindro(raio, altura);
    const total = 2 * base + lateral;
    const volume = volumeCilindro(raio, altura);

    console.log(`Area base = ${base}`);
    console.log(`Area lateral = ${lateral}`);
    console.log(`Area total = ${total}`);
    console.log(`Volume = ${volume}`);
}

function questao4(t) {
    const raio1 = 24 + t;
    const raio2 = 15 + t;
    const altura = 12;
    const geratriz = 15;

    const base1 = areaCirculo(raio1);
    const base2 = areaCirculo(raio2);
    const lateral = areaLateralTroncoCone(raio1, raio2, geratriz);
    const volume = volumeTroncoCone(raio1, raio2, altura);

    console.log(`Geratriz = ${geratriz}`);
    console.log(`Area base1 = ${base1}`);
    console.log(`Area base2 = ${base2}`);
    console.log(`Area lateral = ${lateral}`);
    console.log(`Area total = ${base1 + base2 + lateral}`);
    console.log(`Volume = ${volume}`);
}

function prova(t) {
    questao1(t);
    console.log('---------------------------------');
    // questao2(t);
    // console.log('---------------------------------');
    // questao3(t);
    // console.log('---------------------------------');
    // questao4(t);
}

prova(40);