interface ISaudacao {
    bomDia(): string;
}


class Pessoa implements ISaudacao {
    private readonly nome;

    constructor(nome: string) {
        this.nome = nome;
    }

    bomDia(): string {
        return `Bom dia ${this.nome}`;
    }

}

const p: Pessoa = new Pessoa('Gorete');

console.log(p.bomDia());