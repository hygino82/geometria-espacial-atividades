const f = (x) => 4 * x + 7;
const g = (x) => 5 * x;

const x = 10;

console.log(`f(g(${x})) = ${f(g(x))}`);
console.log(`g(f(${x})) = ${g(f(x))}`);