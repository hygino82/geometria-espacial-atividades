import os
from formulas import *


def questao1(t: int):
    raio = 17 + t
    altura = 25 + t
    geratriz = geratrizCone(raio, altura)
    base = areaCirculo(raio)
    lateral = areaLateralCone(raio, geratriz)
    total = lateral + base
    volume = volumeCone(raio, altura)
    print("Questão 1")
    print(f"raio = {raio}")
    print(f"altura = {altura}")
    print(f"geratriz = {geratriz}")
    print(f"base = {base}")
    print(f"lateral = {lateral}")
    print(f"total = {total}")
    print(f"volume = {volume}")


def questao2(t):
    geratriz = 14 + t
    raio = geratriz / 2.0
    altura = alturaCone(raio, geratriz)
    base = areaCirculo(raio)
    lateral = areaLateralCone(raio, geratriz)
    total = lateral + base
    volume = volumeCone(raio, altura)
    print("Questão 2")
    print(f"raio = {raio}")
    print(f"altura = {altura}")
    print(f"geratriz = {geratriz}")
    print(f"base = {base}")
    print(f"lateral = {lateral}")
    print(f"total = {total}")
    print(f"volume = {volume}")


def questao3(t):
    altura = 36 * t
    raio = 9 * t
    base = areaCirculo(raio)
    lateral = areaLateralCilindo(raio, altura)
    total = lateral + 2 * base
    volume = volumeCilindo(raio, altura)
    print("Questão 3")
    print(f"raio = {raio}")
    print(f"altura = {altura}")
    print(f"base = {base}")
    print(f"lateral = {lateral}")
    print(f"total = {total}")
    print(f"volume = {volume}")


def questao4(t):
    raio1 = 24 + t
    raio2 = 15 + t
    altura = 12
    geratriz = 15
    base1 = areaCirculo(raio1)
    base2 = areaCirculo(raio2)
    lateral = areaLateralTroncoCone(geratriz, raio1, raio2)
    total = base1 + base2 + lateral
    volume = volumeTroncoCone(raio1, raio2, altura)
    print("Questão 4")
    print(f"raio1 = {raio1}")
    print(f"raio2 = {raio2}")
    print(f"altura = {altura}")
    print(f"geratriz = {geratriz}")
    print(f"base1 = {base1}")
    print(f"base2 = {base2}")
    print(f"lateral = {lateral}")
    print(f"total = {total}")
    print(f"volume = {volume}")


def resolverProva(t):
    questao1(t)
    print("*" * 30)
    questao2(t)
    print("*" * 30)
    questao3(t)
    print("*" * 30)
    questao4(t)
    print("*" * 30)


t = 10
while True:
    t = int(input("Informe o número do aluno -> "))
    if t > 0 and t < 40:
        os.system("clear") or None
        print(f"t = {t}")
        resolverProva(t)
    else:
        break
